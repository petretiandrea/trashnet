#!/bin/bash
SERVICE_NAME=scanbage
export FIREBASE_TOKEN=$(cat /root/firebase_token.txt)
export MONGOOSE_TOKEN=$(cat /root/mongoose_token.txt)
docker-compose rm -s -f "$SERVICE_NAME" # force removal of services described into docker-compose.yaml
docker-compose pull "$SERVICE_NAME"  # update the images described into docker-compose.yaml
docker-compose up "$SERVICE_NAME"