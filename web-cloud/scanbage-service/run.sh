#!/bin/bash
SERVER_NAME=node
/snap/bin/docker container kill "$SERVER_NAME" 
/snap/bin/docker container rm "$SERVER_NAME"
/snap/bin/docker pull asw2019/node-server
/snap/bin/docker run -d -p 8080:3000 --name node -e FIREBASE_TOKEN="$(cat /root/firebase_token.txt)" -e MONGOOSE_TOKEN="$(cat /root/mongoose_token.txt)" --network="scanbagehttps" asw2019/node-server
