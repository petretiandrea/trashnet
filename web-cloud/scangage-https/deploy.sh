#!/bin/bash
SERVICE_NAME=scanbage
SERVICE_REVERSE_PROXY=webservice

docker-compose rm -s -f "$SERVICE_NAME" "$SERVICE_REVERSE_PROXY" 
docker-compose pull "$SERVICE_NAME"
docker-compose up -d "$SERVICE_NAME" "$SERVICE_REVERSE_PROXY"