import numpy as np
import matplotlib
import itertools
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches


def separate_pattern_classes(data, labels, class_count):
    pattern_count = data.shape[0]
    result = np.zeros(class_count, dtype=np.ndarray)
    for i in range(class_count):
        result[i] = np.array([data[j] for j in range(pattern_count) if labels[j] == i])

    return result


def frange(x, y, step):
    while x < y:
        yield x
        x += step


def get_colors_from_colormap(colorMap, step):
    cmap = matplotlib.cm.get_cmap('Spectral')

    colors = []
    for i in frange(0, 1 + step, step):
        colors.append(matplotlib.colors.to_hex(cmap(i)))

    return colors


def calculate_2D_min_max(dataset, border):
    xData = [d[0] for d in dataset]
    yData = [d[1] for d in dataset]

    xMin = np.amin(xData) - border
    xMax = np.amax(xData) + border
    yMin = np.amin(yData) - border
    yMax = np.amax(yData) + border

    return xMin, xMax, yMin, yMax


def calculate_2D_decision_boundary_map(classifier, xMin, xMax, yMin, yMax, step):
    xRange = np.arange(xMin, xMax, step)
    yRange = np.arange(yMin, yMax, step)

    decisionBoundaryMap = np.empty((yRange.shape[0], xRange.shape[0]), dtype=int)

    for i, j in itertools.product(range(yRange.shape[0]), range(xRange.shape[0])):
        p = np.array([[xRange[j], yRange[i]]])
        decisionBoundaryMap[i, j] = int(classifier.predict(p)[0])

    return decisionBoundaryMap, xRange, yRange


def color_fading(colors, fadingFactor):
    fadedColors = []
    for c in colors:
        rgbColor = matplotlib.colors.to_rgb(c)
        fadedRgbColor = [fadingFactor * c1 + (1 - fadingFactor) * c2 for (c1, c2) in zip(rgbColor, (1, 1, 1))]
        fadedColors.append(matplotlib.colors.to_hex(fadedRgbColor))

    return fadedColors


def decisionboundaries_drawing(numFigure, xRange, yRange, decisionBoundaryMap, colorMap):
    plt.figure(numFigure)
    xx, yy = np.meshgrid(xRange, yRange)
    plt.pcolormesh(xx, yy, decisionBoundaryMap, cmap=colorMap, alpha=1)
    plt.draw()


def legend_drawing(numFigure, colors, labels, loc):
    plt.figure(numFigure)
    items = [mpatches.Rectangle((0, 0), 1, 1, fc=c) for c in colors]
    plt.legend(items, labels, loc=loc)
    plt.draw()


def subplot_legend_drawing(numFigure, numRows, numColums, numSubFigure, colors, labels, loc):
    plt.subplot(numRows, numColums, numSubFigure)
    legend_drawing(numFigure, colors, labels, loc)


def subplotting_decisionboundaries_drawing(numFigure, numRows, numColums, numSubFigure, xRange, yRange,
                                           decisionBoundaryMap, colorMap):
    plt.subplot(numRows, numColums, numSubFigure)
    decisionboundaries_drawing(numFigure, xRange, yRange, decisionBoundaryMap, colorMap)


def plotting_patterns(numFigure, patterns, patternColor, patternSize=16, marker='o'):
    plt.figure(numFigure)
    plt.scatter(patterns[:, 0], patterns[:, 1], s=patternSize, c=patternColor, marker=marker, edgecolors='#000000')
    plt.draw()


def subplotting_patterns(numFigure, numRows, numColums, numSubFigure, patterns, patternColor):
    plt.subplot(numRows, numColums, numSubFigure)
    plotting_patterns(numFigure, patterns, patternColor)


def subplotting_text(numFigure, numRows, numColums, numSubFigure, xPos, yPos, text):
    plt.figure(numFigure)
    plt.subplot(numRows, numColums, numSubFigure)
    plt.text(xPos, yPos, text)
    plt.draw()


def subplotting_title(numFigure, numRows, numColums, numSubFigure, title):
    plt.figure(numFigure)
    subplot_fig = plt.subplot(numRows, numColums, numSubFigure)
    subplot_fig.set_title(title)
    plt.draw()


def load_labeled_dataset(filePath, featureCount):
    data = np.loadtxt(filePath, usecols=range(0, featureCount))
    labels = np.loadtxt(filePath, usecols=[featureCount])

    return data, labels
	
def show_2D_results(classifier, *args):
    feature_count = args[0][0].shape[1]
    # Solo per il training set con 2 features
    if feature_count == 2:
        # Disegno dei pattern e delle aree di probabilità
        all_patterns_coordinates = np.concatenate([args[i][0] for i in range(len(args))])

        x_min, x_max, y_min, y_max = calculate_2D_min_max(all_patterns_coordinates, 10)
        decision_boundary_map, x_range, y_range = calculate_2D_decision_boundary_map(classifier,
																					x_min, x_max,
																					y_min, y_max, 2)

        plt.figure(num=1, figsize=(18, 8), dpi=96)

        colors = ["red", "coral", "gold", "yellowgreen", "green", "mediumaquamarine",
                  "mediumturquoise", "cornflowerblue", "blue", "purple"]
        faded_colors = color_fading(colors, 0.7)
        color_map = matplotlib.colors.ListedColormap(faded_colors)

        legend_labels = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]

        for i in range(len(args)):
            draw_patterns_decision_boundaries(args[i][0], args[i][1], 10, 1, 1, 3, i+1, x_range, y_range,
                                              decision_boundary_map, color_map, colors, 
                                              args[i][2], faded_colors, legend_labels)
        plt.show()
    else:
        print('show_results può visualizzare solamente pattern di bidimensionali')
		
def draw_patterns_decision_boundaries(data, labels, num_class, num_figure, num_rows, num_columns, num_sub_figure,
                                      x_range, y_range, z, decision_boundary_colors, pattern_colors, title,
                                      legend_colors, legend_labels):
    subplotting_decisionboundaries_drawing(num_figure, num_rows, num_columns, num_sub_figure,
                                                  x_range, y_range, z, decision_boundary_colors)
    subplot_legend_drawing(num_figure, num_rows, num_columns, num_sub_figure, legend_colors, legend_labels, 3)
    separate_class_data = separate_pattern_classes(data, labels, num_class)
    for i in range(num_class):
        subplotting_patterns(num_figure, num_rows, num_columns, num_sub_figure,
                                    separate_class_data[i], pattern_colors[i])

    subplotting_title(num_figure, num_rows, num_columns, num_sub_figure, title)
